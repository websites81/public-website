const html = document.querySelector("html");
const section = document.querySelector("section");
const palmer = document.querySelector(".palmer");
const mountain = document.querySelector(".mountain");
const cultery = document.querySelector(".cultery");
const friends = document.querySelector(".friends");
const footer_arrow = document.querySelector(".footer_arrow");

document.addEventListener("scroll", (e) => {
  if (html.scrollTop >= section.clientHeight * 4.1) {
    footer_arrow.style.animation = "footer_arrow_appearance 4s forwards";
  } else if (html.scrollTop >= section.clientHeight * 3.5) {
    friends.style.animation = "friends_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight * 2.5) {
    cultery.style.animation = "cultery_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight * 1.5) {
    mountain.style.animation = "mountain_appearance 1.5s forwards";
  } else if (html.scrollTop >= section.clientHeight / 2) {
    palmer.style.animation = "palmer_appearance 1.5s forwards";
  }
});
